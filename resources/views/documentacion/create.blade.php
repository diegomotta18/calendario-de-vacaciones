@extends('layouts.app') 
@section('content')
<div class="page-header">
    <h1 class="page-title">Agregar Documentación</h1>
</div>
<div class="page-content container-fluid">
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('documentacion.store') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                
                <div class="row">
                    @if ($errors->any())
                    <div class="col-12">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                    <div class="col-2">
                        <div class="form-group">
                            <label for="user_id">Usuario:</label>
                            <select name="user_id" id="user_id" class="form-control" required>
                                <option value="select" selected disabled>Seleccione...</option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2 p15">
                        <div class="form-group">
                            <label for="date">Fecha archivo: </label>
                            <input type="text" name="date" id="date" placeholder="mm/yyyy" class="form-control datepicker" required value="{{ old('date') }}">
                        </div>
                    </div>
                    <div class="col-2">
                        <label for="file_type">Tipo de archivo:</label>
                        <select name="file_type" id="file_type" class="form-control" required>
                            <option value="select" selected disabled>Seleccione</option>
                            <option value="irpf">IRPF</option>
                            <option value="mod145">Modelo 145</option>
                            <option value="nomina">Nómina</option>
                            <option value="other">Otro</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="title">Título del archivo <small>(opcional)</small></label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>
                    </div>
                    <div class="col-2 p15">
                        <div class="form-group">
                            <label for="file">Archivo:</label>
                            <input id="file" name="file" type="file" class="form-control-file" required>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button class="btn btn-primary pull-right">
                        Cargar Documentación
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
 
@push('styles')
<link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('global/vendor/summernote/summernote.min.css')}}">
<style>
    .datepicker {
        z-index: 1501 !important;
    }
</style>
@endpush 
@push('scripts')
<script src="{{ asset('global/vendor/bootstrap-datepicker/bootstrap-datepicker.es.min.js') }}"></script>
<script src="{{asset('global/js/Plugin/bootstrap-datepicker.js')}}"></script>
<script>
    $(document).ready(function () {
            $('.datepicker').datepicker({
                language: 'es',
                minViewMode: 'months',
                format: 'mm/yyyy'
            });
        });
</script>
@endpush