<ul class="site-menu" data-plugin="menu">
    <li class="dropdown site-menu-item has-sub {{ set_active('home', 'active') }}">
        <a data-toggle="dropdown" href="{{route('home')}}" data-dropdown-toggle="false"
           class=" waves-effect waves-classic">
            <i class="site-menu-icon icon wb-dashboard" aria-hidden="true"></i>
            <span class="site-menu-title">Home</span>
        </a>
    </li>
    <li class="dropdown site-menu-item has-sub {{ set_active('nominas', 'active') }}">
        <a data-toggle="dropdown" href="{{route('nominas.index')}}" data-dropdown-toggle="false"
           class=" waves-effect waves-classic">
            <i class="site-menu-icon icon  wb-layout" aria-hidden="true"></i>
            <span class="site-menu-title">Nóminas</span>
        </a>
    </li>
    <li class="dropdown site-menu-item has-sub {{ set_active('viajes', 'active') }}">
        <a data-toggle="dropdown" href="{{route('viajes.index')}}" data-dropdown-toggle="false"
           class=" waves-effect waves-classic">
            <i class="site-menu-icon icon  wb-globe" aria-hidden="true"></i>
            <span class="site-menu-title">Solicitudes de viaje</span>
        </a>
    </li>
    @if(auth()->user()->hasRole('admin'))
        <li class="dropdown site-menu-item has-sub {{ set_active('documentacion', 'active') }}">
            <a data-toggle="dropdown" href="{{route('documentacion.index')}}" data-dropdown-toggle="false"
               class=" waves-effect waves-classic">
                <i class="site-menu-icon icon  wb-file" aria-hidden="true"></i>
                <span class="site-menu-title" style="max-width: 200px;">Documentación de usuarios</span>
            </a>
        </li>
        <li class="dropdown site-menu-item has-sub {{ set_active('usuarios', 'active') }}">
            <a data-toggle="dropdown" href="{{route('users.index')}}" data-dropdown-toggle="false"
               class=" waves-effect waves-classic">
                <i class="site-menu-icon icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title" style="max-width: 200px;">Usuarios</span>
            </a>
        </li>
    @endif
    <li class="dropdown site-menu-item has-sub {{ set_active('beneficios', 'active') }}">
        <a data-toggle="dropdown" href="{{route('beneficios.index')}}" data-dropdown-toggle="false"
           class=" waves-effect waves-classic">
            <i class="site-menu-icon icon  wb-book" aria-hidden="true"></i>
            <span class="site-menu-title" style="max-width: 200px;">Beneficios</span>
        </a>

    </li>
    <li class="dropdown site-menu-item has-sub {{ set_active('vacaciones', 'active') }}">
        <a data-toggle="dropdown" href="{{route('vacaciones.index')}}" data-dropdown-toggle="false"
           class=" waves-effect waves-classic">
            <i class="site-menu-icon icon  wb-calendar" aria-hidden="true"></i>
            <span class="site-menu-title">Vacaciones</span>
        </a>
    </li>
</ul>
