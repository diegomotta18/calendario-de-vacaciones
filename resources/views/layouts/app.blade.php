<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <link rel="apple-touch-icon" href="{{asset('/topbar/assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('/topbar/assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/global/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.css')}}">
    <link rel="stylesheet" href="{{asset('/topbar/assets/css/site.css')}}">


    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('/global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">

    <!-- Plugins For This Page -->

    {{--<link rel="stylesheet" href="https://rawgit.com/monkbroc/fullcalendar/year-view-demo/dist/fullcalendar.css">--}}
    <link rel="stylesheet" href="{{asset('topbar/calendar/fullcalendar.css')}}">

    {{--<link rel="stylesheet" href="{{asset('topbar/fullcalendar/scheduler.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('topbar/fullcalendar/fullcalendar.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('topbar/fullcalendar/fullcalendar.print.css')}}">--}}

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>--}}
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/jquery-selective/jquery-selective.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/select2/select2.css')}}">
    <!-- Page -->
    <link rel="stylesheet" href="{{asset('/topbar/assets/examples/css/apps/calendar.css')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/global/fonts/web-icons/web-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/global/fonts/brand-icons/brand-icons.css')}}">
    <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">
    <link rel='stylesheet' href="{{asset('topbar/daterangepicker/daterangepicker.css')}}">

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.css"/>


    <link href="{{ asset('/topbar/toastr/toastr.min.css')}}" rel="stylesheet"/>
    <!-- Scripts -->
    <script src="{{ asset('global/vendor/breakpoints/breakpoints.js') }}"></script>

    <script>
        Breakpoints();
    </script>
    <style>
        .fc .fc-toolbar > * > :first-child {
            margin-left: -32px;
        }

        .fc-timelineYear-button {
            padding: 8px 14px;
            font-size: 14px;
            text-transform: capitalize;
            background-image: none;
            border-color: #e4eaec;
            outline: none;
            box-shadow: none;
            background-color: #fff;
            -webkit-appearance: button;
        }

        .fc button {
            height: 40px;
        }

    </style>

    @stack('styles')
</head>
<body class="animsition site-navbar-small page-aside-fixed page-aside-left">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse navbar-inverse"
     role="navigation"
     role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand navbar-brand-center p0" href="{{ route('home') }}">
            <img class="navbar-brand-logo navbar-brand-logo-normal" src="{{asset('/img/Logo_Jabbar-color.png')}}"
                 title="Remark">
            <img class="navbar-brand-logo navbar-brand-logo-special" src="{{asset('/img/Logo_Jabbar-color.png')}}"
                 title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> </span>
        </a>

    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->

            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up" role="button">

                        <span class="icon wb-user"></span>
                    </a>
                    @auth
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                <i class="icon wb-user" aria-hidden="true"></i>
                                {{Auth::user()->name}}
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" role="menuitem"><i class="icon wb-power"
                                                                                             aria-hidden="true"></i>
                                Salir</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>

                        </div>
                    @endauth

                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->

        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon wb-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>

</nav>

<div id="app" class="page">
    <div class="site-menubar site-menubar-light" style="height: fit-content !important;">
        <div class="site-menubar-body">
            @include('layouts.partials.menu-lateral')
        </div>
    </div>

    <div class="page-main">
        @yield('content')
    </div>
</div>

</body>

<script src="{{asset('global/vendor/jquery/jquery.js')}}"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/fc-3.2.3/fh-3.1.3/r-2.2.0/sc-1.4.3/sl-1.2.3/datatables.min.js"></script>

<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{asset('global/vendor/animsition/animsition.js')}}"></script>
<script src="{{asset('global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="{{asset('global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="{{asset('global/vendor/intro-js/intro.js')}}"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>

<!-- Plugins For This Page -->
<script src="{{asset('global/vendor/jquery-ui/jquery-ui.js')}}"></script>
{{--<script src="{{asset('global/vendor/moment/moment.js')}}"></script>--}}
{{--<script src="{{asset('global/vendor/fullcalendar/fullcalendar.js')}}"></script>--}}
<script src="{{asset('global/vendor/jquery-selective/jquery-selective.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-touchspin/bootstrap-touchspin.js')}}"></script>
<script src="{{asset('global/vendor/bootbox/bootbox.js')}}"></script>

<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('topbar/assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('topbar/assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('topbar/assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('topbar/assets/js/Plugin/menu.js')}}"></script>

<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('topbar/assets/js/config/tour.js')}}"></script>


<!-- Page -->
<script src="{{asset('topbar/assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/vendor/select2/select2.full.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>

<script src="{{asset('global/js/Plugin/select2.js')}}"></script>
<script src="{{asset('global/vendor/multi-select/jquery.multi-select.js')}}"></script>
<script src="{{asset('global/js/Plugin/bootstrap-touchspin.js')}}"></script>
<script src="{{asset('global/js/Plugin/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('global/js/Plugin/material.js')}}"></script>
<script src="{{asset('global/js/Plugin/action-btn.js')}}"></script>
<script src="{{asset('global/js/Plugin/editlist.js')}}"></script>
<script src="{{asset('global/js/Plugin/bootbox.js')}}"></script>

<script src="{{asset('topbar/assets/js/Site.js')}}"></script>
<script src="{{asset('topbar/assets/js/App/Calendar.js')}}"></script>

<script src="{{asset('topbar/jquery-validation/dist/jquery.validate.js')}}"></script>
<script src="{{asset('topbar/jquery-validation/dist/additional-methods.js')}}"></script>
<script src="{{asset('topbar/jquery-validation/dist/localization/messages_es.js')}}"></script>
<script src="{{asset('topbar/jquery-validation/dist/localization/methods_es_CL.js')}}"></script>

<script src="{{asset('topbar/assets/examples/js/apps/calendar.js')}}"></script>

<script src="{{asset('topbar/fullcalendar/moment.min.js')}}"></script>

<script src="{{asset('topbar/calendar/fullcalendar.js')}}"></script>
<script src="{{asset('topbar/calendar/lang-all.js')}}"></script>

@if(isset($calendar))
    {!! $calendar->script() !!}
@endif
<script src="{{asset('topbar/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('topbar/daterange/dist/locales/bootstrap-datepicker.es.min.js')}}"></script>
<script src="{{asset('topbar/toastr/toastr.min.js')}}"></script>

{{--<script src="{{asset('js/datatables/viajes.js')}}"></script>--}}

<script src="{{mix('js/components.js')}}"></script>

{{--<script src="{{ asset('/topbar/datatables.js')}}"></script>--}}

<script>
    @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    toastr.options.timeOut = 5000;
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.showMethod = 'slideDown';
    switch (type) {
        case 'info':
            toastr.info("{!!  Session::get('message')  !!}");
            break;
        case 'warning':
            toastr.warning("{!!  Session::get('message')  !!}");
            break;

        case 'success':
            toastr.success("{!!  Session::get('message')  !!}");
            break;

        case 'error':
            toastr.error("{!!  Session::get('message')  !!}");
            break;
    }
    @endif

//    $('#calendar').fullCalendar({
//        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source'
//    });
</script>

@stack('scripts')

</html>
