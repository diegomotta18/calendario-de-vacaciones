@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{asset('topbar/summernote/summernote-lite.css')}}">

@endpush
@section('content')

    <div class="page">
        <div class="page-content container-fluid">
            <div class="row">

                <div class="col-md-12 col-sm-10" id="beneficios">
                    <div>
                        <div class="panel panel-bordered panel-h350">
                            <div class="panel-body" style="max-height: 300px;overflow-y: scroll;">
                                <div class="list-group calendar-list">
                                    <h5 class="">Beneficios sociales</h5>

                                    @foreach($beneficios as $beneficio)
                                        <div class="list-group-item"
                                             data-title="Meeting" data-stick="true" data-color="red-600">
                                            <div class="row">
                                                <input type="hidden" value="{{$beneficio->descripcion}}"
                                                       id="description">
                                                <div class="col-md-4">
                                                    <div style="cursor: pointer;"><i
                                                                class="wb-medium-point red-600 mr-10"
                                                                aria-hidden="true"></i>{{$beneficio->titulo}}
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul>
                                                        @if(auth()->user()->hasRole('admin'))
                                                            @foreach($beneficio->users()->get() as $user)

                                                                <li class="mr-10">{{$user->name}}</li>
                                                            @endforeach
                                                        @endif
                                                        @if(auth()->user()->hasRole('user'))
                                                            @foreach($beneficio->users()->get() as $user)
                                                                @if($user->id == auth()->user()->id)
                                                                    <li class="mr-10">{{$user->name}}</li>
                                                                @endif
                                                            @endforeach
                                                        @endif

                                                    </ul>

                                                </div>
                                                <div class="col-md-4">
                                                    <inputdescription description="{{$beneficio->descripcion}}"
                                                                      title="{{$beneficio->titulo}}"
                                                                      idben="{{$beneficio->id}}"
                                                                      rol="{{auth()->user()->hasRole('admin')}}" users="{{$beneficio->users()->get()}}"></inputdescription>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-sm-10">
                    <div class="panel panel-bordered panel-h350">
                        <div class="panel-body">
                            <h5 class="">Descripción del beneficio</h5>
                            <div class="form-group">
                                <label id="descripcionb" class="form-control-label"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addNewCalendar" aria-hidden="true" aria-labelledby="addNewCalendar"
             role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <form class="modal-content form-horizontal" method="post" action="{{route('beneficios.store')}}"
                      role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Alta de beneficios</h4>
                    </div>
                    <div class="modal-body">
                        @if ($errors->any())
                            <div class="col-12">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <input type="text" name="titulo" class="form-control" placeholder="titulo">
                        </div>
                        <div class="form-group row">
                            <textarea name="descripcion" id="summernote" class="summernote form-control"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="input-group">
                                <span class="input-group-addon"><i style="margin-left: -7px;" class="fa fa-list"></i></span>
                                <select multiple data-plugin="select2" id="user" name="users[]"  class="select2">
                                    <!--<option value="-1" selected="">&#45;&#45; Seleccionar usuario&#45;&#45;</option>-->
                                    @foreach( $users as $user)
                                        <option value="{{$user->id}}">
                                            {{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Aceptar</button>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"
                               href="javascript:void(0)">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="modaleditben" aria-hidden="true"
             role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
                <form id="modaleditbenform" class="modal-content form-horizontal"
                >
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Editar datos del beneficio</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="beneficio_id" name="beneficio_id">

                        <div class="form-group row">
                            <input type="text" id="titulo" name="titulo" class="titulo form-control"
                                   placeholder="titulo">
                        </div>
                        <div class="form-group row">
                            <textarea name="descripcion" id="summernote" class="summernote form-control"></textarea>

                        </div>
                        <div class="form-group row">
                            <div class="input-group">
                                <span class="input-group-addon"><i style="margin-left: -7px;"
                                                                   class="fa fa-list"></i></span>
                                <select multiple data-plugin="select2" name="users[]" id="users" class="select2">
                                    <!--<option value="-1" selected="">&#45;&#45; Seleccionar usuario&#45;&#45;</option>-->
                                    @foreach( $users as $user)
                                        <option value="{{$user->id}}" >
                                            {{ $user->name }}

                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-actions">
                            <button class="btn btn-primary" id="modaleditbenbutton">Aceptar</button>
                            <a class="btn btn-sm btn-default" data-dismiss="modal"
                               href="javascript:void(0)">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="site-action" data-plugin="actionBtn">
            <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
                <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
            </button>
        </div>
    </div>

@endsection
@push('scripts')
<script src="{{asset('topbar/summernote/summernote-lite.js')}}"></script>
<script>


    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#addNewCalendar').on('shown.bs.modal', function () {
            $('#addNewCalendar #summernote').summernote({
                height: 300,
                focus: true,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

        });
        $('#modaleditben').on('shown.bs.modal', function () {
            $('#modaleditben #summernote').summernote({
                height: 300,
                focus: true,
                popover: {
                    image: [],
                    link: [],
                    air: []
                }
            });

        });
        $('#modaleditbenbutton').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: '/beneficios/' + $('#beneficio_id').val() + '/update',
                type: 'PUT',
                data: {
                    titulo: $('#modaleditben #titulo').val(),
                    descripcion: $('#modaleditben #summernote').val(),
                    id: $('#beneficio_id').val(),
                    users: $("#modaleditben #modaleditbenform #users").val()
                },
                success: function (response) {
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function (resp) {
                $('#modaleditben').modal('toggle');
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.success("Se ha modificado correctamente el beneficio");
                setTimeout(function () {
                    window.location.reload();
                    ;
                }, 1000);

            });

        });

        @if (count($errors) > 0)
            $('#addNewCalendar').modal('show');

        @endif
    });

</script>
@endpush