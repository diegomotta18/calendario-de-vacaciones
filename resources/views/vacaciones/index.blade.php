@extends('layouts.app')
@section('content')

    <div class="page-header">
        <h1 class="page-title">Vacaciones</h1>
    </div>
    <div class="page-content container-fluid" id="container-fluid">
        <!-- Panel Basic -->
        <div class="row">
            <div class="col-md-3">
                <div class="panel">

                    <div class="form-group row">
                        <div class="d-flex flex-column" style="margin-left: 25px;">
                            @if(auth()->user()->hasRole('admin'))
                            <div class="form-group">
                                <label class="form-control-label" >Año</label>
                                <input class="datepicker form-control" placeholder="aaaa" id="year">
                            </div>
                            <div class="form-group">
                                <user></user>
                            </div>
                            @endif
                            <div class="radio-custom radio-primary">
                                <input id="approve" type="radio" name="gender" value="approve">
                                <label for="approve">Aprobados</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="notapprove" class="" type="radio" name="gender"
                                       value="notapprove">
                                <label for="notapprove">No aprobadas</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="deny" class="" type="radio" name="gender" value="deny">
                                <label for="deny">Denegados</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="all" class="" type="radio" name="gender" value="all">
                                <label for="all">Todos</label>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-body">
                        <table id="vacaciones" class="table table-hover dataTable table-striped w-full">
                            <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Periodo de vacaciones</th>
                                <th>Aprobar</th>
                                <th>Denegar</th>
                                <th>Acción</th>

                            </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addNewCalendar" aria-hidden="true" aria-labelledby="addNewCalendar" role="dialog"
             tabindex="-1">
            <div class="modal-dialog modal-simple">
                <form class="modal-content form-horizontal" method="post" action="{{route('vacaciones.storeb')}}"
                      role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Solicitud de vacaciones</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 form-control-label" for="ename">Periodo de
                                vacaciones:</label>
                            <div class="col-md-6">
                                <input type="text" name="periodo_de_vacaciones" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Solicitar</button>
                            <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="site-action" data-plugin="actionBtn">
            <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
                <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
            </button>
        </div>





        <div class="modal fade modal-fade-in-scale-up" id="exampleNiftyFadeScale" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1"   >
            <div class="modal-dialog modal-simple">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Eliminar la vacación seleccionada?</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                        <input type="hidden" value="" name="holiday_id" id="holiday_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary waves-effect waves-classic" id="btn_eliminar_vacacion">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{asset('global/js/Plugin/bootstrap-datepicker.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            language: 'es',
            minViewMode: 'years',
            format: 'yyyy',
            autoclose: true,

        });
        $('input[name="periodo_de_vacaciones"]').daterangepicker({
            "showDropdowns": true,
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            "opens": "center"
        }, function (start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
    });
</script>

<script>

    jQuery(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var isAdmin = {{ (Auth::user()->hasRole('admin')) }}

        $('#year').on('change',function(){
            if ($('#approve').is(':checked') === true){
                url = "/vacacionesdtapprove";
                load(url);
            }else
            if($('#deny').is(':checked') === true) {
                url = "/vacacionesdtdeny";
                load(url)
            }else
            if($('#all').is(':checked') === true){
                url = "/vacacionesdtall";
                load(url);
            } else
            if($('#notapprove').is(':checked') === true){
                url = "/vacacionesdtall";
                load(url);
            }else{
                url = "/vacacionesforuseryear";
                load(url);
            }
        });

        $('#user').on('change',function(){
            if ($('#approve').is(':checked') === true){
                url = "/vacacionesdtapprove";
                load(url);
            }else
            if($('#deny').is(':checked') === true) {
                url = "/vacacionesdtdeny";
                load(url)
            }else
            if($('#all').is(':checked') === true){
                url = "/vacacionesdtall";
                load(url);
            } else
            if($('#notapprove').is(':checked') === true){
                url = "/vacacionesdtall";
                load(url);
            }else{
                url = "/vacacionesforuseryear";
                load(url);
            }
        });

        $('#approve').change(function () {
            url = "/vacacionesdtapprove";
            load(url);
        });
        $('#deny').change(function () {

            url = "/vacacionesdtdeny";
            load(url)
        });

        $('#all').change(function () {
            url = "/vacacionesdtall";
            load(url);
        });


        $('#notapprove').change(function () {
            url = "/vacacionesdtnotapprove";
            load(url);
        });
        //Carga por defecto todos las vacaciones cuando se ingresa por primera vez a la pagina
        var loadvacacionesTable = function () {
            var table = $('#vacaciones').DataTable({
                responsive: true,
                "ordering": false,
                "destroy": true,
                "searching": false,

                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bLengthChange": true, //thought this line could hide the LengthMenu
                "bInfo": true,
                autoWidth: true,
                language: {
                    url: "/js/datatable_spanish.json"
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "ajax": "/vacacionesdt",
                "columns": [

                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.user.name;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.fecha_desde + ' - ' + JsonResultRow.fecha_hasta;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === "1") {
                                if (isAdmin === 1) {
                                    return '<button type="button" class="approve btn btn-block btn-success">Aprobado</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-block btn-success" disabled>Aprobado</button>'

                                }
                            } else {
                                if (isAdmin === 1) {
                                    return '<button type="button" class="approve btn btn-outline btn-warning">Aprobar</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-outline btn-warning" disabled>Aprobar</button>'
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === 0) {
                                if (isAdmin === 1) {
                                    return '<button type="button"  class="deny btn btn-block btn-danger">Denegado</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-block btn-danger" disabled>Denegado</button>';
                                }
                            } else {
                                if (isAdmin === 1) {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger">Denegar</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger" disabled>Denegar</button>';
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return '<button class="btn btn-info waves-effect waves-classic delete" data-target="#exampleNiftyFadeScale" data-toggle="modal" type="button">Eliminar</button>';

                        }
                    }
                ],
                "scroller": false,
                "scrollY": "600px",
                "scrollCollapse": true
            });


            $("#vacaciones tbody").on("click", "button.delete", function () {
                var data = table.row($(this).parents("tr")).data();
                console.log(data.id);
                $("#exampleNiftyFadeScale #holiday_id").val(data.id);

            });

            $("#vacaciones tbody").on("click", "button.approve", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/vacaciones/approve/' + data.id,
                    success: function (response) {
                        $('#vacaciones').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },

                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.success("Se ha aprobado el periodo de vacaciones");
                });

            });

            $("#vacaciones tbody").on("click", "button.deny", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/vacaciones/deny/' + data.id,
                    success: function (response) {
                        $('#vacaciones').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },
                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.error("Se ha denegado el periodo de vacaciones");
                });

            });
        }
        loadvacacionesTable();

        $('#exampleNiftyFadeScale #btn_eliminar_vacacion').click(function (e) {
            e.preventDefault();
            $.ajax({
                method: 'delete',
                url: '/vacaciones/' + $("#exampleNiftyFadeScale #holiday_id").val(),
                success: function (response) {
                    $('#exampleNiftyFadeScale').modal('toggle');
                    $('.modal-backdrop').remove();
                    loadvacacionesTable();
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.error("Se ha eliminado el periodo de vacaciones");
            });
        });


        var load = function (url) {
            console.log($('#container-fluid #year').val(),$('#container-fluid #user').val());
            var table = $('#vacaciones').DataTable({
                responsive: true,
                "ordering": false,
                "destroy": true,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bLengthChange": true, //thought this line could hide the LengthMenu
                "bInfo": true,
                "searching": false,

                autoWidth: true,
                language: {
                    url: "/js/datatable_spanish.json"
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                ajax:   {
                    "url":  url,
                    "type": "POST",
                    "data": function(data){
                        data.user_id = $('#container-fluid #user').val(),
                        data.year =$('#container-fluid #year').val()
                    }


                },


                "columns": [

                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.user.name;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.fecha_desde + ' - ' + JsonResultRow.fecha_hasta;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === "1") {
                                if (isAdmin) {
                                    return '<button type="button" class="approve btn btn-block btn-success">Aprobado</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-block btn-success" disabled>Aprobado</button>'

                                }
                            } else {
                                if (isAdmin) {
                                    return '<button type="button" class="approve btn btn-outline btn-warning">Aprobar</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-outline btn-warning" disabled>Aprobar</button>'
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === 0) {
                                if (isAdmin) {
                                    return '<button type="button"  class="deny btn btn-block btn-danger">Denegado</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-block btn-danger" disabled>Denegado</button>';
                                }
                            } else {
                                if (isAdmin) {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger">Denegar</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger" disabled>Denegar</button>';
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return '<button class="btn btn-info waves-effect waves-classic delete" data-target="#exampleNiftyFadeScale" data-toggle="modal" type="button">Eliminar</button>';
                        }
                    }
                ],
                "scroller": false,
                "scrollY": "600px",
                "scrollCollapse": true
            });


            $("#vacaciones tbody").on("click", "button.delete", function () {
                var data = table.row($(this).parents("tr")).data();
                console.log(data.id);
                $("#exampleNiftyFadeScale #holiday_id").val(data.id);

            });

            $("#vacaciones tbody").on("click", "button.approve", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/vacaciones/approve/' + data.id,
                    success: function (response) {
                        $('#vacaciones').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },

                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.success("Se ha aprobado el periodo de vacaciones");
                });

            });

            $("#vacaciones tbody").on("click", "button.deny", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/vacaciones/deny/' + data.id,
                    success: function (response) {
                        $('#vacaciones').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },
                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.error("Se ha denegado el periodo de vacaciones");
                });

            });

        }


    });
    /**
     * Created by diego on 17/5/17.
     */
    /**
     * Created by diego on 27/9/17.
     */

</script>
@endpush