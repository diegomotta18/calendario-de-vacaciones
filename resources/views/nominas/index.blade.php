@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Nóminas</h1>
    </div>
    <div class="page-content container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($nominas as $nomina)
                            <tr>
                                <td class="">{{ $nomina->title }}</td>
                                <td>
                                    <div class="form-inline">
                                        <a class="btn btn-sm btn-outline-info" style="margin-right: 10px;"
                                           href="{{ route('documentacion.download', $nomina->id) }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.min.css')}}">
@endpush