@extends('layouts.guest')

@section('content')
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
        <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
            <div class="brand">
                <img class="brand-img" src="{{asset('topbar/assets/images/logo.png')}}" alt="...">
                <h2 class="brand-text">Jabbar</h2>
                <h3 class="brand-text">Reestablecer contraseña</h3>

            </div>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="sr-only">E-Mail</label>
                    <input id="email" type="email" class="form-control" placeholder="E-mail" name="email"
                           value="{{ old('email') }}"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary btn-block">
                    Enviar restablecimiento de contraseña
                </button>
            </form>
        </div>
    </div>

@endsection
