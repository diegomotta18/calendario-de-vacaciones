@component('mail::message')

    Su solicitud de vacaciones desde {{$vacacion->fecha_desde}} hasta {{$vacacion->fecha_hasta}} ha sido denegada.

@endcomponent
