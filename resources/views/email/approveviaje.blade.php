@component('mail::message')

    Su solicitud de viaje desde {{$viaje->fecha_desde}} hasta {{$viaje->fecha_hasta}} ha sido aprobada

@endcomponent