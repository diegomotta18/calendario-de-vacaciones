@component('mail::message')

El usuario **{{$user->name}}**

Solicita los dias de vacaciones desde {{$vacacion->fecha_desde}} hasta {{$vacacion->fecha_hasta}}.

@endcomponent
