@component('mail::message')

    El usuario **{{$user->name}}**

    Solicita los dias de viaje desde {{$viaje->fecha_desde}} hasta {{$viaje->fecha_hasta}}.

@endcomponent
