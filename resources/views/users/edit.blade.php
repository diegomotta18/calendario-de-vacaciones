@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Editar usuario</h1>
    </div>

    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card">

                    <form method="POST" action="{{ route('users.update', $user->id) }}">
                        {!! csrf_field() !!}
                        {!! method_field('PUT') !!}
                        <div class="card-body">

                            @include('users.partials.form')

                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection