@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Crear usuario</h1>
    </div>

    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        <form method="POST" action="{{ route('users.store') }}">
                            {!! csrf_field() !!}

                            @include('users.partials.form')

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection