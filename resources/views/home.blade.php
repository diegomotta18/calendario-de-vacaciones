@extends('layouts.app')
@section('content')
    <div class="page">
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-bordered panel-h350">
                        <div class="panel-body">
                            <div class="card-block" align="center">
                                <a class="avatar avatar-lg" href="javascript:void(0)">
                                    <img src="{{asset('global/5.jpg')}}" alt="...">
                                </a>
                                <h4 class="profile-user">{{auth()->user()->name}}</h4>
                                <p class="profile-job">{{auth()->user()->roles()->first()->name}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-bordered panel-h350">
                        <div class="panel-body">
                            <h5>TUS NOMINAS</h5>
                            <ul class="list-group list-nominas">
                                @foreach (auth()->user()->nominas()->take(5)->get() as $nomina)
                                    <li class="list-group-item pl-0 pr-0 pb-3 text-uppercase">
                                        <a href="{{ route('documentacion.download', $nomina->id) }}">
                                            <i class="fa fa-fw fa-file-o" aria-hidden="true"></i> {{ $nomina->title }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="panel-footer">
                            <a href="{{ route('nominas.index') }}" style="color: darkgray !important;">
                                Ver más nóminas...
                            </a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>


                <div class="col-sm-6 col-md-3">
                    <div class="col p0">
                        <div class="panel panel-bordered panel-beneficios panel-h350">
                            <div class="panel-body">
                                <h4 align="center">BENEFICIOS SOCIALES </h4>
                                @if(!is_null(Auth::User()->beneficios()->get()))
                                    <div style="max-height: 240px;overflow-y: scroll;">
                                        @foreach(Auth::User()->beneficios()->get() as $beneficio)
                                            <div class="list-group-item  ui-draggable ui-draggable-handle"
                                                 data-title="Meeting" data-stick="true" data-color="red-600"
                                                 style="padding: 0px;">
                                                <i
                                                        class="wb-medium-point white-600 mr-10"
                                                        aria-hidden="true"></i>{{$beneficio->titulo}}

                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div class="center-block text-center" align="center">
                                        <a href="{{route('beneficios.index')}}">Ver todos los beneficios</a>
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>

                @if(auth()->user()->hasFiscalDocs())
                    <div class="col-sm-6 col-md-3">

                        <div class="col p0">
                            <div class="panel panel-bordered panel-documentos panel-h350">
                                <div class="panel-body">
                                    <h4>DOCUMENTOS FISCALES 2017</h4>
                                    @if(auth()->user()->hasIrpf())
                                        <h1 class="m0" style="margin-top:-10px;">
                                            <a class="irpf"
                                               href="{{ route('documentacion.download', auth()->user()->last_irpf->id) }}">
                                                IRPF
                                            </a>
                                        </h1>
                                    @endif
                                    @if(auth()->user()->hasMod145())
                                        <h1 class="m0" style="margin-top:-10px;">
                                            <a class="mod145"
                                               href="{{ route('documentacion.download', auth()->user()->last_mod145->id) }}">
                                                MODELO 145
                                            </a>
                                        </h1>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-bordered">
                            <div class="panel-body">
                                <div class="calendar-container">
                                    <div class="col-md-3 col-sm-2">
                                        <section class="page-aside-section">
                                            <h4 class="m0">CALENDARIOS</h4>
                                            <h5 class="m0 text-uppercase">Vacaciones y cumpleaños</h5>
                                            <div class="list-group calendar-list">
                                                @if(!is_null($users))

                                                    <a class="list-group-item calendar-event"
                                                       data-title="Meeting"
                                                       data-stick=true data-color="red-600"
                                                       href="{{route('vacaciones.foralluser')}}">
                                                        <i class="wb-medium-point red-600 mr-10"
                                                           aria-hidden="true"></i>Ver
                                                        todos los usuarios

                                                    </a>
                                                    @foreach($users as $user)
                                                        <a class="list-group-item calendar-event"
                                                           data-title="Meeting"
                                                           data-stick=true data-color="red-600"
                                                           href="{{route('vacaciones.foruser',$user->id)}}">
                                                            <i class="wb-medium-point red-600 mr-10"
                                                               aria-hidden="true"></i>{{$user->name}}
                                                        </a> @endforeach
                                                @endif
                                            </div>
                                        </section>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="calendar-container">
                                            {!! $calendar->calendar() !!}
                                            <div class="modal fade" id="addNewCalendar" aria-hidden="true"
                                                 aria-labelledby="addNewCalendar" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-simple">
                                                    <form class="modal-content form-horizontal" method="post"
                                                          action="{{route('vacaciones.store')}}" role="form">
                                                        <input type="hidden" name="_token"
                                                               value="{{ csrf_token() }}">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    aria-hidden="true"
                                                                    data-dismiss="modal">×
                                                            </button>
                                                            <h4 class="modal-title">Solicitud de vacaciones</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group row">
                                                                <label class="col-md-4 form-control-label"
                                                                       for="ename">Periodo
                                                                    de
                                                                    vacaciones:</label>
                                                                <div class="col-md-6">
                                                                    <input type="text"
                                                                           name="periodo_de_vacaciones"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="form-actions">
                                                                <button class="btn btn-primary" type="submit">
                                                                    Solicitar
                                                                </button>
                                                                <a class="btn btn-sm btn-white"
                                                                   data-dismiss="modal"
                                                                   href="javascript:void(0)">Cancelar</a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Site Action -->
        <div class="site-action" data-plugin="actionBtn">
            <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
                <i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
            </button>
        </div>
        <!-- End Site Action -->
    </div>
@endsection

@push('scripts')
<script src="{{asset('global/js/Plugin/bootstrap-datepicker.js')}}"></script>
<script>
    $(document).ready(function () {

        $('input[name="periodo_de_vacaciones"]').daterangepicker({
            "showDropdowns": true,
            "autoApply": true,
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Apply",
                "cancelLabel": "Cancel",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },
            "opens": "center"
        }, function (start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
    });
</script>
@endpush