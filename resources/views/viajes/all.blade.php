@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Solicitud de viajes</h1>
    </div>
    <div class="page-content container-fluid">
        <!-- Panel Basic -->
        <div class="row">
            <div class="col-md-3">
                <div class="panel">

                    <div class="form-group row">
                        <div class="d-flex flex-column" style="margin-left: 25px;">
                            <div class="radio-custom radio-primary">
                                <input id="approve" type="radio" name="gender" value="approve">
                                <label for="approve">Aprobados</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="notapprove" class="" type="radio" name="gender"
                                       value="notapprove">
                                <label for="notapprove">No aprobadas</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="deny" class="" type="radio" name="gender" value="deny">
                                <label for="deny">Denegados</label>

                            </div>
                            <div class="radio-custom radio-primary">
                                <input id="all" class="" type="radio" name="gender" value="all">
                                <label for="all">Todos</label>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-body">
                        <table id="viajes" class="table table-hover dataTable table-striped w-full">
                            <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Periodo de viaje</th>
                                <th>Aprobar</th>
                                <th>Denegar</th>
                                <th>Acciones</th>

                            </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="site-action" data-plugin="actionBtn">
                <a href="{{route('viajes.create')}}" class="site-action-toggle btn-raised btn btn-success btn-floating">
                    <i style="margin-top: 9px;" class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                    <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>

    jQuery(document).ready(function ($) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var isAdmin = {{ (Auth::user()->hasRole('admin')) }}


        $('#approve').change(function () {
            url = "/viajesdtapprove";
            load(url);
        });
        $('#deny').change(function () {

            url = "/viajesdtdeny";
            load(url)
        });

        $('#all').change(function () {
            url = "/viajesdtall";
            load(url);
        });


        $('#notapprove').change(function () {
            url = "/viajesdtnotapprove";
            load(url);
        });
//Carga por defecto todos las viajes cuando se ingresa por primera vez a la pagina
        var loadviajesTable = function () {
            var table = $('#viajes').DataTable({
                responsive: true,
                "ordering": false,
                "searching": false,
                "destroy": true,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bLengthChange": true, //thought this line could hide the LengthMenu
                "bInfo": true,
                autoWidth: true,
                language: {
                    url: "/js/datatable_spanish.json"
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "ajax": "/viajesdt",
                "columns": [

                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.user.name;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.fecha_desde + ' - ' + JsonResultRow.fecha_hasta;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === "1") {
                                if (isAdmin === 1) {
                                    return '<button type="button" class="approve btn btn-block btn-success">Aprobado</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-block btn-success" disabled>Aprobado</button>'

                                }
                            } else {
                                if (isAdmin === 1) {
                                    return '<button type="button" class="approve btn btn-outline btn-warning">Aprobar</button>'
                                } else {
                                    return '<button type="button" class="approve btn btn-outline btn-warning" disabled>Aprobar</button>'
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === 0) {
                                if (isAdmin === 1) {
                                    return '<button type="button"  class="deny btn btn-block btn-danger">Denegado</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-block btn-danger" disabled>Denegado</button>';
                                }
                            } else {
                                if (isAdmin === 1) {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger">Denegar</button>';
                                } else {
                                    return '<button type="button"  class="deny btn btn-outline  btn-danger" disabled>Denegar</button>';
                                }
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return '<button type="button"  class="show btn btn-outline btn-info">Visualizar</button>';

                        }
                    }
                ],
                "scroller": false,
                "scrollY": "600px",
                "scrollCollapse": true
            });


            $("#viajes tbody").on("click", "a.delete", function () {
                var data = table.row($(this).parents("tr")).data();
                $("#frmDeleteViaje #id").val(data.id);

            });

            $("#viajes tbody").on("click", "button.approve", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/viajes/approve/' + data.id,
                    success: function (response) {
                        $('#viajes').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },

                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.success("Se ha aprobado el periodo de viajes");
                });

            });

            $("#viajes tbody").on("click", "button.deny", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/viajes/deny/' + data.id,
                    success: function (response) {
                        $('#viajes').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },
                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.error("Se ha denegado el periodo de viajes");
                });

            });


            $("#viajes tbody").on("click", "button.show", function () {
                var data = table.row($(this).parents("tr")).data();
                console.log(data);
                window.location.href = "/viajes/" + data.id;
            });

        }
        loadviajesTable();

        $('#frmDeleteViaje #btn_eliminar_buy').click(function (e) {
            e.preventDefault();
            console.log($("#frmDeleteViaje #id").val());
            $.ajax({
                method: 'delete',
                url: '/viajes/' + $("#frmDeleteViaje #id").val(),
                success: function (response) {
                    $('#modalDelete').modal('toggle');
                    $('.modal-backdrop').remove();
                    loadviajesTable();
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.error("Se ha eliminado el periodo de viajes");
            });
        });


        var load = function (url) {
            var table = $('#viajes').DataTable({
                responsive: true,
                "destroy": true,
                "ordering": false,
                "searching": false,
                "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                "bLengthChange": true, //thought this line could hide the LengthMenu
                "bInfo": true,
                autoWidth: true,
                language: {
                    url: "/js/datatable_spanish.json"
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "ajax": url,
                "columns": [

                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.user.name;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            return JsonResultRow.fecha_desde + ' - ' + JsonResultRow.fecha_hasta;
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            if (JsonResultRow.fecha_aprobada === "1") {
                                return '<button type="button" class="approve btn btn-block btn-success">Aprobado</button>'
                            } else {
                                return '<button type="button" class="approve btn btn-outline btn-warning">Aprobar</button>'
                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {

                            if (JsonResultRow.fecha_aprobada === 0) {
                                return '<button type="button"  class="deny btn btn-block btn-danger">Denegado</button>';
                            } else {
                                return '<button type="button"  class="deny btn btn-outline  btn-danger">Denegar</button>';

                            }
                        }
                    },
                    {
                        "render": function (data, type, JsonResultRow, meta) {
                            if (JsonResultRow.deleted_at === null ) {

                                return '<button type="button"  class="show btn btn-outline btn-info">Visualizar</button>';
                            }else{
                                return '<button type="button"  class="show btn btn-outline btn-info" disabled>Visualizar</button>';
                            }
                        }
                    }
                ],
                "scroller": false,
                "scrollY": "600px",
                "scrollCollapse": true
            });


            $("#viajes tbody").on("click", "a.delete", function () {
                var data = table.row($(this).parents("tr")).data();
                $("#frmDeleteViaje #id").val(data.id);

            });

            $("#viajes tbody").on("click", "button.approve", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/viajes/approve/' + data.id,
                    success: function (response) {
                        $('#viajes').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },

                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.success("Se ha aprobado el periodo de viajes");
                });

            });

            $("#viajes tbody").on("click", "button.deny", function (e) {
                e.preventDefault();
                var data = table.row($(this).parents("tr")).data();
                $.ajax({
                    method: 'post',
                    url: '/viajes/deny/' + data.id,
                    success: function (response) {
                        $('#viajes').DataTable().ajax.reload()
                    },
                    error: function (error) {
                        console.log(error);
                    },
                }).done(function () {
                    toastr.options.timeOut = 5000;
                    toastr.options.closeButton = true;
                    toastr.error("Se ha denegado el periodo de viajes");
                });

            });

            $("#viajes tbody").on("click", "button.show", function () {
                var data = table.row($(this).parents("tr")).data();
                console.log(data);
                window.location.href = "/viajes/" + data.id;
            });

        }


    });
    /**
     * Created by diego on 17/5/17.
     */
    /**
     * Created by diego on 27/9/17.
     */
</script>
@endpush