@extends('layouts.app')
@section('content')
    @push('styles')
    <style>
        #paso2, #paso3, #paso4 {
            display: none;
        }
        @media only screen and (min-width : 992px) {
            #numero_de_personas{
                width: 80px;
            }
        }

    </style>
    @endpush
    {{--<wizardviaje></wizardviaje>--}}
    <div class="page-content container-fluid">
        <div class="page-main">
            <div class="page">
                <div class="page-content container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="panel" id="exampleWizardForm">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Que necesitas para tu viaje</h3>
                                </div>

                                <div class="panel-body">
                                    <!-- Steps -->
                                    <div class="steps steps-sm row" data-plugin="matchHeight" data-by-row="true"
                                         role="tablist">
                                        <div id="pa1" class="step col-lg-3 "
                                             data-target="#exampleAccount" role="tab">
                                            <span class="step-number">1</span>
                                            <div class="step-desc">
                                                <span class="step-title">IDENTIFICATE </span>
                                                <p>Necesitamos tus datos para confirmaciones</p>
                                            </div>
                                        </div>

                                        <div id="pa2"  class="step col-lg-3" data-target="#exampleBilling" role="tab">
                                            <span class="step-number">2</span>
                                            <div class="step-desc">
                                                <span class="step-title">VIAJE: BILLETE TREN O AVIÓN </span>
                                                <p>Indica la hora aproximada y fecha</p>
                                            </div>
                                        </div>

                                        <div id="pa3" class="step col-lg-3" data-target="#exampleGetting" role="tab">
                                            <span class="step-number">3</span>
                                            <div class="step-desc">
                                                <span class="step-title">NECESITAS HOTEL,TRASLADO O COCHE?</span>
                                                <p>Indica el dia y la hora de llegada aprox.</p>
                                            </div>
                                        </div>
                                        <div id="pa4" class="step col-lg-3" data-target="#exampleGetting"
                                        >
                                            <span class="step-number">4</span>
                                            <div class="step-desc">
                                                <span class="step-title">ENVIA TU SOLICITUD</span>
                                                <p>Confirma tu solicitud, en breve respondemos</p>
                                            </div>
                                        </div>
                                    </div>

                                    <form id="paso1" class="">
                                        <div class="wizard-content">
                                            <div class="wizard-pane active" id="exampleAccount" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="form-control-label"
                                                                   for="nombre">Nombre</label>
                                                            <input type="text" class="form-control"
                                                                   id="nombre"
                                                                   name="nombre[]" required
                                                            >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="apellido"
                                                                   class="form-control-label">Apellido</label>
                                                            <input type="text" class="form-control"
                                                                   id="apellido"
                                                                   name="apellido[]"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                               <inputviaje></inputviaje>
                                            </div>
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary" id="siguiente1">
                                                    Siguiente
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                    <form id="paso2">
                                        <div class="wizard-content">
                                            <div class="row">
                                                <div class="form-group">
                                                    <h4>Rango de fechas</h4>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="radio-custom radio-primary">
                                                            <input id="fecha_exacta" type="radio" name="gender">
                                                            <label for="fecha_exacta">Fecha exacta</label>

                                                        </div>
                                                        <p>
                                                            Si sabes una fecha exacta en la que tienes que viajar
                                                            indica la IDA y la vuelta</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <div id="ida_vuelta">
                                                            <div class="input-daterange input-group"
                                                                 id="datepicker">
                                                                <input type="text" class="form-control" id="fecha_de_ida"
                                                                       name="fecha_de_id">
                                                                <span class="input-group-addon">hasta</span>
                                                                <input type="text" class="form-control" id="fecha_de_vuelta"
                                                                       name="fecha_de_vuelta">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="radio-custom radio-primary">
                                                            <input id="rango_de_fecha" type="radio" name="gender">
                                                            <label for="rango_de_fecha">Rango de fechas</label>

                                                        </div>
                                                        <p>Si tienes que viajar pero no tienes cerrada una fecha en
                                                            concreto indica de qué dias a qué dias podemos buscar
                                                            disponibilidad</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <div id="desde_hasta">
                                                            <div class="input-daterange input-group"
                                                                 id="datepicker">
                                                                <input type="text" class="form-control" id="desde"
                                                                       name="desde">
                                                                <span class="input-group-addon">hasta</span>
                                                                <input type="text" class="form-control" id="hasta"
                                                                       name="hasta">
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-icon">
                                                        <span class="input-group-addon" style="width: 30px;"><span
                                                                    style="margin-left:-6px;"
                                                                    class="wb wb-map"></span></span>
                                                            <input type="text" id="localizacion" name="localizacion"
                                                                   class="form-control"
                                                                   placeholder="Indica tu localización" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group input-group-icon">
                                                        <span class="input-group-addon" style="width: 30px;"><span
                                                                    style="margin-left:-6px;"
                                                                    class="wb wb-map"></span></span>
                                                            <input type="text" id="destino" name="destino"
                                                                   class="form-control"
                                                                   placeholder="Indica tu destino" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: -0.571rem;">
                                                        <div class="checkbox-custom checkbox-primary">
                                                            <input type="checkbox"
                                                                   name="hotel"
                                                                   id="hotel"
                                                            >
                                                            <label for="hotel">Necesitas hotel?</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: -0.571rem;">
                                                        <div class="checkbox-custom checkbox-primary">
                                                            <input type="checkbox"
                                                                   name="traslado"
                                                                   id="traslado"
                                                            >
                                                            <label for="traslado">Necesitas traslado?</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: -0.571rem;">
                                                        <div class="checkbox-custom checkbox-primary">
                                                            <input type="checkbox"
                                                                   name="s"
                                                                   id="coche"
                                                            >
                                                            <label for="coche">Necesitas alquilar coche?</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-bottom: -0.571rem;">
                                                        <div class="checkbox-custom checkbox-primary">
                                                            <input type="checkbox" id="tren" name="tren"
                                                            >
                                                            <label for="tren">Tren</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="checkbox-custom checkbox-primary">
                                                            <input type="checkbox" id="avion" name="avion"
                                                            >
                                                            <label for="avion">Avión</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <button type="button" class="btn btn-default" id="atras1">
                                                    Atras
                                                </button>
                                                <button type="button" class="btn btn-primary" id="siguiente2">
                                                    Siguiente
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <form id="paso3">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h4 class="example-title" align="center">Fecha de hotel</h4>
                                                <div class="form-group">
                                                    <div class="input-group input-group-icon">
                                                        <span class="input-group-addon" style="width: 30px;"><span
                                                                    style="margin-left:-6px;"
                                                                    class="wb wb-map"></span></span>
                                                        <input type="text" class="form-control" align="center"
                                                               id="fecha_de_hotel"
                                                               name="fecha_de_hotel" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <h4 class="example-title" align="center">
                                                    Fecha de alquiler de coche</h4>
                                                <div class="form-group">
                                                    <div class="input-group input-group-icon">
                                                        <span class="input-group-addon" style="width: 30px;"><span
                                                                    style="margin-left:-6px;"
                                                                    class="wb wb-map"></span></span>
                                                        <input align="center" type="text" class="form-control"
                                                               id="fecha_de_alquiler_de_coche"
                                                               name="fecha_de_alquiler_de_coche" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <button type="button" class="btn btn-default" id="atras2">
                                                Atras
                                            </button>
                                            <button type="button" class="btn btn-primary" id="siguiente3">
                                                Siguiente
                                            </button>
                                        </div>
                                    </form>
                                    <form id="paso4">
                                        <div class="row">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <!-- Example Basic Form (Form grid) -->
                                                    <div class="example-wrap">
                                                        <h4 class="example-title">Detalle de la solicitud de viaje</h4>
                                                        <div class="example">
                                                            <form autocomplete="off">

                                                                <label class="form-control-label"><b>Acompañantes:</b></label>
                                                                <div class="row">
                                                                    <div class="form-group form-material col-md-6">
                                                                        <input type="text" class="form-control" id="acompanante"

                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <hr>
                                                                </div>

                                                                <div class="row" id="fecha_exacta">
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Fecha de ida</label>
                                                                        <input type="text" class="form-control" id="fecha_de_ida"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Fecha de vuelta</label>
                                                                        <input type="text" class="form-control" id="fecha_de_vuelta"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Localización</label>
                                                                        <input type="text" class="form-control" id="localizacion"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Destino</label>
                                                                        <input type="text" class="form-control" id="destino"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                </div>
                                                                <div id="rango_de_fecha">

                                                                    <label class="form-control-label"><b>Rango de fechas:</b></label>
                                                                    <hr>
                                                                    <label>Si tienes que viajar pero no tienes cerrada una fecha en
                                                                        concreto indica de qué dias a qué dias podemos buscar
                                                                        disponibilidad</label>
                                                                    <div class="row">

                                                                        <div class="form-group form-material col-md-6">
                                                                            <label class="form-control-label"
                                                                                   for="inputBasicFirstName">Desde</label>
                                                                            <input type="text" class="form-control" id="desde"
                                                                                   name="inputFirstName"
                                                                                   autocomplete="off"
                                                                                   disabled>
                                                                        </div>
                                                                        <div class="form-group form-material col-md-6">
                                                                            <label class="form-control-label"
                                                                                   for="inputBasicFirstName">Hasta</label>
                                                                            <input type="text" class="form-control" id="hasta"
                                                                                   name="inputFirstName"
                                                                                   autocomplete="off"
                                                                                   disabled>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="example-wrap">
                                                        <div class="example">
                                                            <form>
                                                                <div class="row">

                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Hotel</label>
                                                                        <input type="text" class="form-control" id="hotel"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Traslado </label>
                                                                        <input type="text" class="form-control" id="traslado"
                                                                               name="inputFirstName" placeholder="First Name"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Coche</label>
                                                                        <input type="text" class="form-control" id="coche"
                                                                               name="inputFirstName" placeholder=""
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Tren</label>
                                                                        <input type="text" class="form-control" id="tren"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                    <div class="form-group form-material col-md-6">
                                                                        <label class="form-control-label"
                                                                               for="inputBasicFirstName">Avión</label>
                                                                        <input type="text" class="form-control" id="avion"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>


                                                                </div>
                                                                <label class="form-control-label"><b>Fechas de hotel:</b></label>
                                                                <hr>
                                                                <div class="row">

                                                                    <div class="form-group form-material col-md-6">

                                                                        <input type="text" class="form-control" id="fecha_hotel"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>
                                                                </div>
                                                                <label class="form-control-label"><b>Fechas de coche:</b></label>
                                                                <hr>
                                                                <div class="row">

                                                                    <div class="form-group form-material col-md-6">

                                                                        <input type="text" class="form-control" id="fecha_coche"
                                                                               name="inputFirstName"
                                                                               autocomplete="off"
                                                                               disabled>
                                                                    </div>

                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-default" id="atras3">Atras
                                                </button>
                                            </div>
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary" id="confirmar" value="Confirmar">Confirmar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script src="{{asset('js/wizard.js')}}"></script>

@endpush