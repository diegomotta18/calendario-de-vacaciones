$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#paso4 #rango_de_fecha').hide();

    $("#desde_hasta .input-daterange").datepicker({
        language: "es",
        format: "dd/mm/yyyy",
        autoclose: true
    });
    $("#ida_vuelta .input-daterange").datepicker({
        language: "es",
        format: "dd/mm/yyyy",
        autoclose: true
    });
    $("#fechahotel").datepicker({
        autoclose: true,
        weekStart: 1,
        format: "dd/mm/yyyy",
        language: "es"
    });
    $("#fechacoche").datepicker({
        autoclose: true,
        weekStart: 1,
        language: "es",
        format: "dd/mm/yyyy"
    });


    $('input[name="fecha_de_hotel"]').daterangepicker(
        {

            "autoApply": true,
            locale: {
                format: "DD/MM/YYYY",
                separator: " - ",
                applyLabel: "Aplicar",
                cancelLabel: "Cancelar",
                fromLabel: "Desde",
                toLabel: "hasta",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],

                firstDay: 1
            },
            opens: "center"
        },
        function (start, end, label) {
            console.log(
                "New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')"
            );
        }
    );

    $('input[name="fecha_de_alquiler_de_coche"]').daterangepicker(
        {

            "autoApply": true,
            locale: {
                format: "DD/MM/YYYY",
                separator: " - ",
                applyLabel: "Apply",
                cancelLabel: "Cancel",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                weekLabel: "W",
                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],

                firstDay: 1
            },
            opens: "center"
        },
        function (start, end, label) {
            console.log(
                "New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')"
            );
        }
    );
    $('#pa1').addClass('current');
    $("#siguiente1").click(function () {
        var form = $("#paso1");
        form.validate({
            errorElement: 'span',
            errorClass: 'help-block',
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').addClass("has-error");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass("has-error");
            },
            rules: {
                nombre: {
                    required: true,
                    minlength: 6,
                },
                apellido: {
                    required: true,
                    minlength: 6,
                },
                nom: {
                    required: true,
                    minlength: 6,
                },
                ape: {
                    required: true,
                    minlength: 6,
                },
                fecha_de_ida: {
                    required: true,
                },
                fecha_de_vuelta: {
                    required: true,
                },
            },
            messages: {
                nombre: {
                    required: "El nombre es requerido",
                },
                apellido: {
                    required: "El apellido es requerido",
                },
                nom: {
                    required: "El nombre es requerido",
                },
                ape: {
                    required: "El apellido es requerido",
                },
            }
        });
        if (form.valid() == true) {
            current_fs = $('#paso1');
            next_fs = $('#paso2');
            $('#pa1').removeClass('current');
            $('#pa2').addClass('current');
            next_fs.show();
            current_fs.hide();

        }
    });

    $("#siguiente2").click(function () {
        var form = $("#paso2");
        $.validator.addMethod("greaterThan", function (value, element) {
            return moment($('#fecha_de_ida').val(), 'YYYY-MM-DD').toDate() < moment(value, 'YYYY-MM-DD').toDate();
        }, "Fecha de vuelta debe ser mayor a la fecha de ida");
        if ($('#fecha_exacta').is(':checked')==true){
            form.validate({
                errorClass: 'help-block',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group').addClass("has-error").append(error);
                    console.log(element);
                },

                rules: {
                    destino: {
                        required: true,
                        minlength: 6,
                    },
                    localizacion: {
                        required: true,
                        minlength: 6,
                    },
                    fecha_de_ida: {
                        required: true,
                    },
                    fecha_de_vuelta: {
                        required: true,
                    },
                },
                messages: {
                    destino: {
                        required: "El destino es requerido",
                    },
                    localizacion: {
                        required: "El localización es requerido",
                    },

                    fecha_de_ida: {
                        required: "La fecha exacta es requerida",
                    },
                    fecha_de_vuelta: {
                        required: "La fecha exacta es requerida",
                    }
                }
            });
        }else if($('#rango_de_fecha').is(':checked')==true)
        {
            form.validate({
                errorClass: 'help-block',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group').addClass("has-error").append(error);
                    console.log(element);
                },

                rules: {
                    destino: {
                        required: true,
                        minlength: 6,
                    },
                    localizacion: {
                        required: true,
                        minlength: 6,
                    },
                    desde: {
                        required: true,
                    },
                    hasta: {
                        required: true,
                    },
                },
                messages: {
                    destino: {
                        required: "El destino es requerido",
                    },
                    localizacion: {
                        required: "El localización es requerido",
                    },

                    desde: {
                        required: "El rango de fecha es requerido",
                    },
                    hasta: {
                        required: "El rango de fecha es requerido",
                    }
                }
            });
        }else{
            form.validate({
                errorClass: 'help-block',
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    $(element).closest('.form-group').addClass("has-error").append(error);
                    console.log(element);
                },

                rules: {
                    destino: {
                        required: true,
                        minlength: 6,
                    },
                    localizacion: {
                        required: true,
                        minlength: 6,
                    },
                    fecha_de_ida: {
                        required: true,
                    },
                    fecha_de_vuelta: {
                        required: true,
                    },
                },
                messages: {
                    destino: {
                        required: "El destino es requerido",
                    },
                    localizacion: {
                        required: "El localización es requerido",
                    },

                    fecha_de_ida: {
                        required: "La fecha exacta es requerida",
                    },
                    fecha_de_vuelta: {
                        required: "La fecha exacta es requerida",
                    },
                }
            });
        }
        if (form.valid() == true) {

            if($('#hotel').is(':checked')===true || $('#traslado').is(':checked')===true || $('#coche').is(':checked')===true || $('#avion').is(':checked')===true || $('#tren').is(':checked')===true ){
                current_fs = $('#paso2');
                next_fs = $('#paso3');
                $('#pa2').removeClass('current');
                $('#pa3').addClass('current');
                next_fs.show();
                current_fs.hide();
                if ($('#fecha_de_vuelta').val() && $('#fecha_de_ida').val()) {

                    $('input[name="fecha_de_hotel"]').daterangepicker(
                        {
                            "startDate": $('#fecha_de_ida').val(),
                            "endDate": $('#fecha_de_vuelta').val(),
                            "autoApply": true,
                            locale: {
                                format: "DD/MM/YYYY",
                                separator: " - ",
                                applyLabel: "Apply",
                                cancelLabel: "Cancel",
                                fromLabel: "From",
                                toLabel: "To",
                                customRangeLabel: "Custom",
                                weekLabel: "W",
                                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                                monthNames: [
                                    "Enero",
                                    "Febrero",
                                    "Marzo",
                                    "Abril",
                                    "Mayo",
                                    "Junio",
                                    "Julio",
                                    "Agosto",
                                    "Septiembre",
                                    "Octubre",
                                    "Noviembre",
                                    "Diciembre"
                                ],

                                firstDay: 1
                            },
                            opens: "center"
                        });
                    $('input[name="fecha_de_alquiler_de_coche"]').daterangepicker(
                        {
                            "startDate": $('#fecha_de_ida').val(),
                            "endDate": $('#fecha_de_vuelta').val(),
                            "autoApply": true,
                            locale: {
                                format: "DD/MM/YYYY",
                                separator: " - ",
                                applyLabel: "Apply",
                                cancelLabel: "Cancel",
                                fromLabel: "From",
                                toLabel: "To",
                                customRangeLabel: "Custom",
                                weekLabel: "W",
                                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                                monthNames: [
                                    "Enero",
                                    "Febrero",
                                    "Marzo",
                                    "Abril",
                                    "Mayo",
                                    "Junio",
                                    "Julio",
                                    "Agosto",
                                    "Septiembre",
                                    "Octubre",
                                    "Noviembre",
                                    "Diciembre"
                                ],

                                firstDay: 1
                            },
                            opens: "center"
                        })
                } else if ($('#desde').val() && $('#hasta').val()) {
                    $('input[name="fecha_de_hotel"]').daterangepicker(
                        {
                            "startDate": $('#desde').val(),
                            "endDate": $('#hasta').val(),
                            "autoApply": true,
                            locale: {
                                format: "DD/MM/YYYY",
                                separator: " - ",
                                applyLabel: "Apply",
                                cancelLabel: "Cancel",
                                fromLabel: "From",
                                toLabel: "To",
                                customRangeLabel: "Custom",
                                weekLabel: "W",
                                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                                monthNames: [
                                    "Enero",
                                    "Febrero",
                                    "Marzo",
                                    "Abril",
                                    "Mayo",
                                    "Junio",
                                    "Julio",
                                    "Agosto",
                                    "Septiembre",
                                    "Octubre",
                                    "Noviembre",
                                    "Diciembre"
                                ],

                                firstDay: 1
                            },
                            opens: "center"
                        });
                    $('input[name="fecha_de_alquiler_de_coche"]').daterangepicker(
                        {
                            "startDate": $('#desde').val(),
                            "endDate": $('#hasta').val(),
                            "autoApply": true,
                            locale: {
                                format: "DD/MM/YYYY",
                                separator: " - ",
                                applyLabel: "Apply",
                                cancelLabel: "Cancel",
                                fromLabel: "From",
                                toLabel: "To",
                                customRangeLabel: "Custom",
                                weekLabel: "W",
                                daysOfWeek: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                                monthNames: [
                                    "Enero",
                                    "Febrero",
                                    "Marzo",
                                    "Abril",
                                    "Mayo",
                                    "Junio",
                                    "Julio",
                                    "Agosto",
                                    "Septiembre",
                                    "Octubre",
                                    "Noviembre",
                                    "Diciembre"
                                ],

                                firstDay: 1
                            },
                            opens: "center"
                        })
                }
            }else{
                current_fs = $('#paso2');
                next_fs = $('#paso4');
                $('#pa2').removeClass('current');
                $('#pa4').addClass('current');
                next_fs.show();
                current_fs.hide();
                $('input[name="fecha_de_hotel"]').val(null);
                $('input[name="fecha_de_alquiler_de_coche"]').val(null);
                $('#paso4 #acompanante').val( $('#numero_de_personas').val());
                $('#paso4 #fecha_de_ida').val($('#fecha_de_ida').val());
                $('#paso4 #fecha_de_vuelta').val( $('#fecha_de_vuelta').val());
                if($('#hotel').is(":checked")){
                    $('#paso4 #hotel').val("Si");
                }else{
                    $('#paso4 #hotel').val("No");
                }
                if($('#coche').is(":checked")){
                    $('#paso4 #coche').val("Si");
                }else{
                    $('#paso4 #coche').val("No");
                }
                if($('#tren').is(":checked")){
                    $('#paso4 #tren').val("Si");
                }else{
                    $('#paso4 #tren').val("No");
                }
                if($('#avion').is(":checked")){
                    $('#paso4 #avion').val("Si");
                }else{
                    $('#paso4 #avion').val("No");
                }
                $('#paso4 #fecha_hotel').val($('#fecha_de_hotel').val());
                $('#paso4 #fecha_coche').val($('#fecha_de_alquiler_de_coche').val());
                $('#paso4  #localizacion').val($('#localizacion').val());
                $('#paso4 #destino').val( $('#destino').val());
                if ($('#desde').val() !== null && $('#hasta').val() !== null){
                    $('#paso4 #rango_de_fecha').show();
                    $('#paso4 #desde').val($('#desde').val());
                    $('#paso4 #hasta').val($('#hasta').val());
                }
            }


        }
    });

    $('#fecha_de_vuelta').attr('disabled',true);
    $('#fecha_de_ida').attr('disabled',true);
    $('#desde').attr('disabled',true);
    $('#hasta').attr('disabled',true);
    $('#fecha_exacta').on('click',function () {
        if ($(this).is(':checked')) {
            // append goes here
            $('#desde').attr('disabled',true);
            $('#hasta').attr('disabled',true);
            $('#fecha_de_vuelta').attr('disabled',false);
            $('#fecha_de_ida').attr('disabled',false);
        }
    });

    $('#rango_de_fecha').on('click',function () {
        if ($(this).is(':checked')) {
            // append goes here
            $('#fecha_de_vuelta').attr('disabled',true);
            $('#fecha_de_ida').attr('disabled',true);
            $('#desde').attr('disabled',false);
            $('#hasta').attr('disabled',false);
        }
    })
    
    $('#desde').on('change', function () {
        $('#ida #fecha_de_ida').val("");
        $('#vuelta #fecha_de_vuelta').val("");
    });

    $('#fecha_de_vuelta').on('change', function () {
        $('#desde').val("");
        $('#hasta').val("");
    });

    $('#fecha_de_ida').on('change', function () {
        $('#desde').val("");
        $('#hasta').val("");
    });
    $('#atras1').click(function () {
        current_fs = $('#paso2');
        next_fs = $('#paso1');
        $('#pa2').removeClass('current');
        $('#pa1').addClass('current');
        next_fs.show();
        current_fs.hide();
    });

    $('#atras2').click(function () {
        current_fs = $('#paso3');
        next_fs = $('#paso2');
        $('#pa3').removeClass('current');
        $('#pa2').addClass('current');
        next_fs.show();
        current_fs.hide();
    });

    $('#atras3').click(function () {
        current_fs = $('#paso4');
        next_fs = $('#paso3');
        $('#pa4').removeClass('current');
        $('#pa3').addClass('current');
        next_fs.show();
        current_fs.hide();


    });

    $("#siguiente3").click(function () {

        var form = $("#paso3");
        form.validate({
            errorClass: 'help-block',
            errorElement: 'span',
            errorPlacement: function (error, element) {
                $(element).closest('.form-group').addClass("has-error").append(error);
                console.log(element);
            },

            rules: {
                fecha_de_hotel: {
                    required: true,
                },
                fecha_de_alquiler_de_coche: {
                    required: true,
                },

            },
            messages: {
                fecha_de_hotel: {
                    required: "Las fechas de hotel es requerido",
                },
                fecha_de_alquiler_de_coche: {
                    required: "Las fechas de alquiler de coche es requerido",
                },
            }
        });
        if (form.valid() === true) {
            current_fs = $('#paso3');
            next_fs = $('#paso4');
            $('#pa3').removeClass('current');
            $('#pa4').addClass('current');
            next_fs.show();
            current_fs.hide();

            $('#paso4 #acompanante').val( $('#numero_de_personas').val());
            $('#paso4 #fecha_de_ida').val($('#fecha_de_ida').val());
            $('#paso4 #fecha_de_vuelta').val( $('#fecha_de_vuelta').val());
            if($('#hotel').is(":checked")){
                $('#paso4 #hotel').val("Si");
            }else{
                $('#paso4 #hotel').val("No");
            }
            if($('#coche').is(":checked")){
                $('#paso4 #coche').val("Si");
            }else{
                $('#paso4 #coche').val("No");
            }
            if($('#traslado').is(":checked")){
                $('#paso4 #traslado').val("Si");
            }else{
                $('#paso4 #traslado').val("No");
            }
            if($('#tren').is(":checked")){
                $('#paso4 #tren').val("Si");
            }else{
                $('#paso4 #tren').val("No");
            }
            if($('#avion').is(":checked")){
                $('#paso4 #avion').val("Si");
            }else{
                $('#paso4 #avion').val("No");
            }
            $('#paso4 #fecha_hotel').val($('#fecha_de_hotel').val());
            $('#paso4 #fecha_coche').val($('#fecha_de_alquiler_de_coche').val());
            $('#paso4  #localizacion').val($('#localizacion').val());
            $('#paso4 #destino').val( $('#destino').val());
            if ($('#desde').val() !== null && $('#hasta').val() !== null){
                $('#paso4 #rango_de_fecha').show();
                $('#paso4 #desde').val($('#desde').val());
                $('#paso4 #hasta').val($('#hasta').val());
            }
        }
    });


    $("#confirmar").click(function () {
        $.ajax({
            url: '/viajes/store',
            type: "post",
            data: {
                numero_de_personas: $('#numero_de_personas').val(),
                nombre: $('input[name="nombre[]"]').serializeArray(),
                apellido: $('input[name="apellido[]"]').serializeArray(),
                ida: $('#fecha_de_ida').val(),
                vuelta: $('#fecha_de_vuelta').val(),
                hotel: $('#hotel').is(":checked"),
                traslado: $('#traslado').is(":checked"),
                coche: $('#coche').is(":checked"),
                tren: $('#tren').is(":checked"),
                avion: $('#avion').is(":checked"),
                fecha_de_hotel: $('#fecha_de_hotel').val(),
                fecha_de_alquiler_de_coche: $('#fecha_de_alquiler_de_coche').val(),
                localizacion: $('#localizacion').val(),
                destino: $('#destino').val(),
                desde: $('#desde').val(),
                hasta: $('#hasta').val(),

            },
            success: function (response) { // What to do if we succeed
                toastr.options.timeOut = 5000;
                toastr.success("Se ha creado la solicitud de viaje");
                $('#paso1').trigger("reset");
                $('#paso2')[0].reset();
                $('#paso3').trigger("reset");
                $('#paso4')[0].reset();
                $("#ape").each(function () {
                    $(this).parent("div").remove();
                }),
                    $("#nom").each(function () {
                        $(this).parent("div").remove();
                    }),
                    current_fs = $('#paso4');
                next_fs = $('#paso1');
                next_fs.show();
                current_fs.hide();
                $('#pa1').addClass('current');
                $('#paso1 #cantidad').prop('checked', false);
                $('#paso1 #numero_de_personas').prop('disabled', true);
                $('#paso1 #numero_de_personas').val("");
                $('#paso1 #removeElement').prop('disabled', true);
                $('#paso1 #add').prop('disabled', true);
                $('#paso4 #rango_de_fecha').hide();


            },
            error: function (response) {
                console.log(response);
            }
        });
    })
});