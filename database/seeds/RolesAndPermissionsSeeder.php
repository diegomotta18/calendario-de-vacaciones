<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create ADMIN permissions
        $admin = Role::create(['name' => 'admin']);

        //Aprobar, denegar o borrar vacaciones de todos los usuarios
        Permission::create(['name' => 'create vacations']);
        Permission::create(['name' => 'deny vacations']);
        Permission::create(['name' => 'delete vacations']);

        //editar y eliminar beneficios en /beneficios.
        Permission::create(['name' => 'update benefits']);
        Permission::create(['name' => 'delete benefits']);

        // set permissions
        $admin->givePermissionTo('create vacations');
        $admin->givePermissionTo('deny vacations');
        $admin->givePermissionTo('delete vacations');
        $admin->givePermissionTo('update benefits');
        $admin->givePermissionTo('delete benefits');

        // Create initial admin
        $admin_user = factory(App\User::class)->create([
            'name'     => 'Admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('admin#*'),
        ]);

        $admin_user->assignRole('admin');


        // create USER role
        Role::create(['name' => 'user']);

        // Create initial users
        $user = factory(App\User::class)->create([
            'name'     => 'User',
            'email'    => 'user@user.com',
            'password' => bcrypt('secret#*'),
        ]);

        $user->assignRole('user');
    }
}
