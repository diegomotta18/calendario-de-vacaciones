<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CretateViajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('viajes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('num_personas')->nullable();
            $table->string('datos_persona');
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable() ;
            $table->string('localizacion');
            $table->string('destino');
            $table->date('rango_fecha_desde')->nullable();
            $table->date('rango_fecha_hasta')->nullable();
            $table->boolean('hotel')->default(false);
            $table->boolean('traslados')->default(false);
            $table->boolean('coche')->default(false);
            $table->boolean('tren')->default(false);
            $table->boolean('avion')->default(false);
            $table->date('hotel_fecha_desde')->nullable();
            $table->date('hotel_fecha_hasta')->nullable();
            $table->date('coche_fecha_desde')->nullable();
            $table->date('coche_fecha_hasta')->nullable();
            $table->boolean('fecha_aprobada')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('viajes');

    }
}
