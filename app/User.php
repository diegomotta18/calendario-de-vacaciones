<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function vacaciones()
    {
        return $this->hasMany(Vacaciones::class);
    }

    public function beneficios()
    {
        return $this->belongsToMany('App\Beneficio');
    }
    public function scopeNominas($query)
    {
        $now = new Carbon();
        return $this->documentacion()->where('file_type', '=', 'nomina')->whereYear('created_at','=',$now->year);
    }

    public function documentacion()
    {
        return $this->hasMany(Documentacion::class);
    }

    public function hasFiscalDocs()
    {
        return $this->documentacion()
            ->where('date', '>=', date('Y'))
            ->where('file_type', '=', 'mod145')
            ->orWhere('file_type', '=', 'irpf')
            ->count() > 0;
    }

    public function hasIrpf()
    {
        return $this->documentacion()
            ->where('date', '>=', date('Y'))
            ->where('file_type', '=', 'irpf')
            ->count() > 0;
    }

    public function hasMod145()
    {
        return $this->documentacion()
            ->where('date', '>=', date('Y'))
            ->where('file_type', '=', 'mod145')
            ->count() > 0;
    }

    public function getLastIrpfAttribute()
    {
        return $this->documentacion()
            ->where('date', '>=', date('Y'))
            ->where('file_type', '=', 'irpf')
            ->orderBy('date', 'DESC')
            ->first();
    }

    public function getLastMod145Attribute()
    {
        return $this->documentacion()
            ->where('date', '>=', date('Y'))
            ->where('file_type', '=', 'mod145')
            ->orderBy('date', 'DESC')
            ->first();
    }

    public function viajes()
    {
        return $this->hasMany(Viajes::class);
    }
}
