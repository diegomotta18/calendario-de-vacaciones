<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Viajes extends Model
{
    //
    //
    use SoftDeletes;

    protected $table = 'viajes';

    protected $fillable = [
        'user_id',
        'num_personas',
        'datos_persona',
        'fecha_desde',
        'fecha_hasta',
        'localizacion',
        'destino',
        'rango_fecha_desde',
        'rango_fecha_hasta',
        'hotel',
        'traslados',
        'coche',
        'tren',
        'avion',
        'hotel_fecha_desde',
        'hotel_fecha_hasta',
        'coche_fecha_desde',
        'coche_fecha_hasta',
        'fecha_aprobada',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getFechaDesdeAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getFechaHastaAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getRangoFechaDesdeAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getRangoFechaHastaAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getHotelFechaHastaAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getHotelFechaDesdeAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getCocheFechaDesdeAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getCocheFechaHastasAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function setHotelAttribute($value)
    {
        if ($value == "true") {
            return $this->attributes['hotel'] = 1;
        } else {
            return $this->attributes['hotel'] = 0;
        }
    }

    public function setTrasladosAttribute($value)
    {
        if ($value == "true") {
            return $this->attributes['traslados'] = 1;
        } else {
            return $this->attributes['traslados'] = 0;
        }
    }

    public function setAvionAttribute($value)
    {
        if ($value == "true") {
            return $this->attributes['avion'] = 1;
        } else {
            return $this->attributes['avion'] = 0;
        }
    }

    public function setTrenAttribute($value)
    {
        if ($value == "true") {
            return $this->attributes['tren'] = 1;
        } else {
            return $this->attributes['tren'] = 0;
        }
    }

    public function setCocheAttribute($value)
    {
        if ($value == "true") {
            return $this->attributes['coche'] = 1;
        } else {
            return $this->attributes['coche'] = 0;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
