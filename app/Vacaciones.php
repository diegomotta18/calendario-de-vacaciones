<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacaciones extends Model
{
    //
    use SoftDeletes;

    protected $table = 'vacaciones';

    protected $fillable = [
        'fecha_desde',
        'fecha_hasta',
        'fecha_aprobada',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getFechaDesdeAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function getFechaHastaAttribute($value)
    {
        return date('d/m/Y', strtotime(str_replace('-', '/', $value)));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
