<?php

namespace App\Http\Controllers;

use App\Beneficio;
use App\Http\Requests\BeneficiosStoreRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class BeneficiosController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->hasRole('user')):
            $beneficios = auth()->user()->beneficios;
        else:
            $beneficios = Beneficio::all();
        endif;
        $users = User::all();
        return view('beneficios.index', compact('beneficios','users'));

    }

    public function create(){
        return view('beneficios.create');
    }

    public function store(BeneficiosStoreRequest $request){
         $be =Beneficio::create([
            'titulo' => $request->input('titulo'),
            'descripcion' => $request->input('descripcion'),
        ]);
        $users = $request->input('users');
        $be->users()->attach($users);
        $be->save();
        $notification = array(
            'message' => 'Se ha creado el nuevo beneficio',
            'alert-type' => 'success'
        );

        return Redirect::to(route('beneficios.index'))->with($notification);
    }


    public function  update($id, BeneficiosStoreRequest $request){
        $beneficio = Beneficio::findOrFail($id);
        $beneficio->titulo = $request->input('titulo');
        $beneficio->descripcion = $request->input('descripcion');
        if(!is_null($beneficio->users()->get())){
            foreach( $beneficio->users()->get() as $user){
                $beneficio->users()->detach($user->id);
                $beneficio->save();
            }
        }
        $beneficio->users()->attach($request->input('users'));
        $beneficio->save();
        $beneficios = Beneficio::paginate();

        return view('beneficios', ['beneficios' => $beneficios])->render();
    }

    public function delete($id)
    {
        $beneficio = Beneficio::findOrFail($id);
        $beneficio->delete();

        return [];
    }

    public function usersj(){

        $user = User::all();
        return $user->toJson();
    }
}
