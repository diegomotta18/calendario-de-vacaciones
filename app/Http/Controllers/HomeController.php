<?php

namespace App\Http\Controllers;

use App\User;
use App\Vacaciones;
use Illuminate\Http\Request;
use Calendar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = null;
        $users = null;
        if (auth()->user()->hasRole('user')) {
            $data = Vacaciones::where('user_id', '=', auth()->user()->id)->with('user:id,name')->orderBy('created_at', 'desc')->get();

        } else {
            $data =   Vacaciones::with('user:id,name')->orderBy('created_at', 'desc')->get();
            $users = User::all();

        }


        $calendar = $this->getData($data);

        return view('home',compact('calendar','users'));
    }


    public function getVacacionesPorUsuario($id){
        $data = Vacaciones::with('user:id,name')->where('user_id',$id)->get();
        $calendar = $this->getData($data);
        $users = User::all();
        return view('home',compact('calendar','users'));
    }

    public function getAllVacaciones(){
        $data = Vacaciones::with('user:id,name')->get();
        $calendar = $this->getData($data);
        $users = User::all();
        return view('home',compact('calendar','users'));
    }

    public function getData($data){
        $events = [];
        if($data->count()) {
            foreach ($data as $key => $value) {
                $color = null;
                if ($value->fecha_aprobada == null){
                    //pinta de rojo si esta en null
                    $color = '#eb6709';
                }else{
                    //pinta de verde si esta aprobado
                    $color = '#28a745';
                }

                $events[] = Calendar::event(
                    $value->user->name,
                    true,
                    date('Y/m/d',strtotime(str_replace('/','-',$value->fecha_desde))),
                    date('Y/m/d',strtotime(str_replace('/','-',$value->fecha_hasta))),
                    null,
                    // Add color and link on event
                    [
                        'color' =>$color,

                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);
        $calendar->setOptions(
            [
                'header' => [
                    'left' => 'prev,next today',
                    'center' => 'title',
                    'right' => 'year,month,agendaWeek,agendaDay',
                ],
                'eventLimit' => true,
                'lang'=> 'es'
            ]
        );
        return $calendar;
    }


}
