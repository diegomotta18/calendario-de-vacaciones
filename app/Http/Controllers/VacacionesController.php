<?php

namespace App\Http\Controllers;

use App\Mail\ApproveVacaciones;
use App\Mail\DennyVacaciones;
use App\Mail\SendNotification;
use App\User;
use App\Vacaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use DataTables;
use Calendar;

class VacacionesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(auth()->user()->hasRole('user')):
            $data = auth()->user()->vacaciones;
        else:
            $data = Vacaciones::with('user:id,name')->get();
        endif;

        $calendar = $this->getData($data);

        return view('vacaciones.index', compact('calendar'));
    }

    //guarda la solicitud de vacaciones del usuario
    public function store(Request $request)
    {
        $user = Auth::user();
        $fechas = explode(" - ", $request->input('periodo_de_vacaciones'));
        $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
        $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
        $vacacion = Vacaciones::create([
            'user_id'     => $user->id,
            'fecha_desde' => $fecha_inicio,
            'fecha_hasta' => $fecha_fin,
        ]);
        $notification = [
            'message'    => 'Se ha creado un nueva solicitud de vacaciones',
            'alert-type' => 'success'
        ];
        Mail::to(config('portal_empleado.PORTAL'))->send(new SendNotification($user, $vacacion));
        return Redirect::to(route('home'))->with($notification);
    }

    public function storeb(Request $request)
    {
        $user = Auth::user();
        $fechas = explode(" - ", $request->input('periodo_de_vacaciones'));
        $fecha_inicio = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
        $fecha_fin = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
        $vacacion = Vacaciones::create([
            'user_id'     => $user->id,
            'fecha_desde' => $fecha_inicio,
            'fecha_hasta' => $fecha_fin,
        ]);
        $notification = [
            'message'    => 'Se ha creado un nueva solicitud de vacaciones',
            'alert-type' => 'success']
        ;
        Mail::to(config('portal_empleado.PORTAL'))->send(new SendNotification($user, $vacacion));
        return Redirect::to(route('vacaciones.index'))->with($notification);
    }

    public function getVacaciones()
    {

        $vacaciones = null;
        if (auth()->user()->hasRole('user')) {
            $vacaciones = Vacaciones::where('user_id', '=', auth()->user()->id)->with('user:id,name')->orderBy('created_at', 'desc');

        } else {
            $vacaciones =   Vacaciones::with('user:id,name')->orderBy('created_at', 'desc');
        }

        return DataTables::eloquent(
            $vacaciones
        )->make(true);
    }

    //remover vacaciones

    public function destroy($id){
        $vacaciones = Vacaciones::find($id);
        $vacaciones->delete();
        return http_response_code(200);
    }

    //Aprueba las vacaciones
    public function approveVacaciones($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $vacacion = Vacaciones::find($id);
            $vacacion->fecha_aprobada = true;
            $vacacion->save();
            $user = User::find($vacacion->user_id);
            Mail::to($user->email)->send(new ApproveVacaciones( $vacacion));
            return $vacacion;
        }
    }

    //Deniega las vacaciones
    public function denyVacaciones($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $vacacion = Vacaciones::find($id);
            $vacacion->fecha_aprobada = false;
            $vacacion->save();
            $vacacion->delete();
            $user = User::find($vacacion->user_id);
            Mail::to($user->email)->send(new DennyVacaciones( $vacacion));
            return $vacacion;
        }
    }

    //obtiene todas las vacaciones
    public function getVacacionesAllDt(Request $data)
    {

        if($data->input('year') != null && $data->input('user_id')!= "-1"){

            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->where('user_id','=',$data->input('user_id'))->whereYear('created_at','=',$data->input('year'))->orderBy('created_at',
            'desc'))->make(true);
        }else if($data->input('year') != null && $data->input('user_id')== "-1"){
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->whereYear('created_at','=',$data->input('year'))->orderBy('created_at',
                'desc'))->make(true);
        }
        else{
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->orderBy('created_at',
                'desc'))->make(true);
        }
    }

    //obtiene todos las vacaciones aprovadas
    public function getVacacionesApproveDt(Request $data)
    {

        if($data->input('year') != null && $data->input('user_id')!= "-1"){
            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                true)->where('user_id','=',$data->input('user_id'))->whereYear('created_at','=',$data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else if($data->input('year') != null && $data->input('user_id')== "-1"){
            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                true)->whereYear('created_at','=',$data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else{
            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                true)->orderBy('created_at', 'desc'))->make(true);
        }


    }

    //obtiene todas las vacaciones denegadas
    public function getVacacionesDenyDt(Request $data)
    {
        if($data->input('year') != null && $data->input('user_id')!= "-1") {

            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->where('fecha_aprobada',
                false)->where('user_id', '=', $data->input('user_id'))->whereYear('created_at', '=', $data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else if($data->input('year') != null && $data->input('user_id')== "-1"){
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->where('fecha_aprobada',
                false)->whereYear('created_at', '=', $data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else{
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->where('fecha_aprobada',
                false)->orderBy('created_at', 'desc'))->make(true);
        }
    }

    //obtiene todas las vacaciones no aprobadas
    public function getVacacionesNotApproveDt(Request $data)
    {
        if($data->input('year') != null && $data->input('user_id')!= "-1") {

            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                null)->where('user_id', '=', $data->input('user_id'))->whereYear('created_at', '=', $data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else if($data->input('year') != null && $data->input('user_id')== "-1"){

            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                null)->whereYear('created_at', '=', $data->input('year'))->orderBy('created_at', 'desc'))->make(true);
        }else{
            return DataTables::eloquent(Vacaciones::with('user:id,name')->where('fecha_aprobada',
                null)->orderBy('created_at', 'desc'))->make(true);
        }
    }

    public function getVacacionesForUser(Request $data){
        if($data->input('year') != null  && $data->input('user_id') != "-1") {

            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->where('user_id','=',$data->input('user_id'))->whereYear('created_at','=',$data->input('year'))->orderBy('created_at',
                'desc'))->make(true);
        }else if($data->input('year') != null  && $data->input('user_id') == "-1"){
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->whereYear('created_at','=',$data->input('year'))->orderBy('created_at',
                'desc'))->make(true);
        }
        else{
            return DataTables::eloquent(Vacaciones::withTrashed()->with('user:id,name')->orderBy('created_at',
                'desc'))->make(true);
        };
    }

    public function getData($data)
    {
        $events = [];
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $color = null;
                if ($value->fecha_aprobada == null) {
                    //pinta de rojo si esta en null
                    $color = '#eb6709';
                } else {
                    //pinta de verde si esta aprobado
                    $color = '#28a745';
                }

                $events[] = Calendar::event(
                    $value->user->name,
                    true,
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->fecha_desde))),
                    date('Y/m/d', strtotime(str_replace('/', '-', $value->fecha_hasta))),
                    null,
                    // Add color and link on event
                    [
                        'color' => $color,
                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);
        $calendar->setOptions(
            [
                'header'     => [
                    'left'   => 'prev,next today',
                    'center' => 'title',
                    'right'  => 'year,month,agendaWeek,agendaDay',
                ],
                'eventLimit' => true,
                'lang'       => 'es'
            ]
        );

        return $calendar;
    }
}
