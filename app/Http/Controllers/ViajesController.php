<?php

namespace App\Http\Controllers;

use App\Http\Requests\ViajesStoreRequest;
use App\Mail\ApproveViaje;
use App\Mail\DennyViajes;
use App\Mail\SendNotificacionViaje;
use App\User;
use App\Viajes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\Mail;


class ViajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('viajes.all');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
        return view('viajes.wizard');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(ViajesStoreRequest $request)
    {
        $nombres = $request->get('nombre');
        $apellidos = $request->get('apellido');
        $personas = [];
        foreach ($nombres as $key => $value) {
            $solicitante = array('nombre' => $value['value'], 'apellido' => $apellidos[$key]['value']);
            array_push($personas, $solicitante);
        }
        $hotel_fecha_desde = null;
        $hotel_fecha_hasta = null;
        $coche_fecha_desde = null;
        $coche_fecha_hasta = null;
        if (!is_null($request->get('fecha_de_hotel'))) {
            $fechas1 = explode(" - ", $request->get('fecha_de_hotel'));
            $hotel_fecha_desde = date('Y-m-d', strtotime(str_replace('/', '-', $fechas1[0])));
            $hotel_fecha_hasta = date('Y-m-d', strtotime(str_replace('/', '-', $fechas1[1])));
            $fechas2 = explode(" - ", $request->get('fecha_de_alquiler_de_coche'));
            $coche_fecha_desde = date('Y-m-d', strtotime(str_replace('/', '-', $fechas2[0])));
            $coche_fecha_hasta = date('Y-m-d', strtotime(str_replace('/', '-', $fechas2[1])));
        }

        $fecha_ida = null;
        if (!is_null($request->get('ida'))) {
            $fecha_ida = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('ida'))));
        }
        $fecha_vuelta = null;
        if (!is_null($request->get('vuelta'))) {
            $fecha_vuelta = date('Y-m-d', strtotime(str_replace('/', '-', trim($request->get('vuelta')))));
        }
        $fecha_desde = null;
        if (!is_null($request->get('desde'))) {
            $fecha_desde = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('desde'))));
        }
        $fecha_hasta = null;
        if (!is_null($request->get('hasta'))) {
            $fecha_hasta = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('hasta'))));
        }
        $user = Auth::user();
        $viaje = Viajes::create([
            'datos_persona' => json_encode($personas),
            'num_personas' => $request->get('numero_de_personas'),
            'fecha_desde' => $fecha_ida,
            'fecha_hasta' => $fecha_vuelta,
            'localizacion' => $request->get('localizacion'),
            'destino' => $request->get('destino'),
            'rango_fecha_desde' => $fecha_desde,
            'rango_fecha_hasta' => $fecha_hasta,
            'hotel' => $request->get('hotel'),
            'traslados' => $request->get('traslado'),
            'coche' => $request->get('coche'),
            'tren' => $request->get('tren'),
            'avion' => $request->get('avion'),
            'hotel_fecha_desde' => $hotel_fecha_desde,
            'hotel_fecha_hasta' => $hotel_fecha_hasta,
            'coche_fecha_desde' => $coche_fecha_desde,
            'coche_fecha_hasta' => $coche_fecha_hasta,
            'user_id' => $user->id
        ]);
        $user->viajes()->save($viaje);
        Mail::to(config('portal_empleado.PORTAL'))->send(new SendNotificacionViaje($user, $viaje));
        return http_response_code(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        //
        $viaje = Viajes::findOrFail($id);
        $user = User::find($viaje->user_id);
        return view('viajes.show', compact('viaje', 'user'))->with('personas', json_decode($viaje->datos_persona, true));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }

    public
    function getViajes()
    {
        $viajes = null;
        if (auth()->user()->hasRole('user')) {
            $viajes = Viajes::where('user_id', '=', auth()->user()->id)->with('user:id,name')->orderBy('created_at', 'desc');

        } else {
            $viajes = Viajes::with('user:id,name')->orderBy('created_at', 'desc');
        }

        return DataTables::eloquent(
            $viajes
        )->make(true);

    }

//Aprueba las vacaciones
    public
    function approveViaje($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $viaje = Viajes::find($id);
            $viaje->fecha_aprobada = true;
            $viaje->save();
            $user = User::find($viaje->user_id);
            Mail::to($user->email)->send(new ApproveViaje($viaje));

            return $viaje;
        }
    }

//Deniega las vacaciones
    public
    function denyViaje($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $viaje = Viajes::find($id);
            $viaje->fecha_aprobada = false;
            $viaje->save();
            $viaje->delete();
            $user = User::find($viaje->user_id);
            Mail::to($user->email)->send(new DennyViajes( $viaje));
            return $viaje;
        }
    }

//obtiene todas los viajes
    public
    function getViajesAllDt()
    {
        return DataTables::eloquent(Viajes::with('user:id,name')->orderBy('created_at', 'desc'))->make(true);
    }

//obtiene todos los viajes aprovadas
    public
    function getViajesApproveDt()
    {
        return DataTables::eloquent(Viajes::with('user:id,name')->where('fecha_aprobada', true)->orderBy('created_at', 'desc'))->make(true);

    }

//obtiene todas los viajes denegadas
    public
    function getViajesDenyDt()
    {
        return DataTables::eloquent(Viajes::withTrashed()->with('user:id,name')->where('fecha_aprobada', false)->orderBy('created_at', 'desc'))->make(true);
    }

//obtiene todas los viajes no aprobadas
    public
    function getViajesNotApproveDt()
    {
        return DataTables::eloquent(Viajes::with('user:id,name')->where('fecha_aprobada', null)->orderBy('created_at', 'desc'))->make(true);
    }
}
