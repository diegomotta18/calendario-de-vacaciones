<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ViajesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'numero_de_personas' => "",
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'ida',
            'vuelta',
            'fecha_de_hotel',
            'fecha_de_alquiler_de_coche',
            'localizacion' => 'required|max:255',
            'destino' => 'required|max:255',
            'desde',
            'hasta',
            'avion',
            'tren',
            'coche',
            'traslado',
            'hotel',
            'numero_de_personas',
        ];
    }
}
